from lupmodel.model_persister import ModelPersister
import torch

class TorchModelPersister(ModelPersister):

    def persist(self, agent, outDir, version):
        torch.save(agent.qnetwork_local.state_dict(), outDir + str(version) + '.pth')