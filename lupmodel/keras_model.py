from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
import tensorflow as tf
import numpy as np
import random
import keras
from collections import deque
import sys
# make sure the root path is in system path
from pathlib import Path
from keras.callbacks import TensorBoard
from tensorboard import program



UPDATE_EVERY = 10  # how often to update the network
BATCH_SIZE = 512  # minibatch size TODO 512
GAMMA = 0.99  # discount factor 0.99
TAU = 1e-3  # for soft update of target parameters

base_dir = Path(__file__).resolve().parent
sys.path.append(str(base_dir))

class KerasModel:

    def __init__(self, state_size, action_size, hidden_layer_1_size=16, hidden_layer_2_size=0, learning_rate=0.5e-4):
        """Initialize an Agent object.
                Params
                ======
                    state_size (int): dimension of each state
                    action_size (int): dimension of each action
                """
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate
        self.hidden_layer_1_size = hidden_layer_1_size
        self.hidden_layer_2_size = hidden_layer_2_size
        self.memory = deque(maxlen=2000)
        self.t_step = 0
        self.model = self.create_model()
        self.target_model = self.create_model()
        self.target_model.summary()
        print("tf version: {}".format(tf.__version__))

        tensorboard=TensorBoard(log_dir='C:\\devsbb\\git\\aicrowd\\baselines\\checkpoints',
                                #histogram_freq=0,  # How often to log histogram visualizations
                                #embeddings_freq=0,  # How often to log embedding visualizations
                                #update_freq='epoch'
                                )
        self.callbacks = [

            #keras.callbacks.ModelCheckpoint(
            #    filepath='C:/devsbb/git/aicrowd/baselines/checkpoints/mymodel_{epoch}.h5',
                # Path where to save the model
                # The two parameters below mean that we will overwrite
                # the current checkpoint if and only if
                # the `val_loss` score has improved.
           #     save_best_only=False,
           #     monitor='val_loss',
           #     verbose=0)
            #tensorboard
        ]
        pass

    def create_model(self) -> Sequential:
        input_shape=np.zeros(self.state_size).shape
        #action_shape=np.zeros(self.action_size).shape
        model = Sequential()
        model.add(Dense(self.hidden_layer_1_size, input_dim=self.state_size, activation="relu"))
        if self.hidden_layer_2_size > 0:
            self.model.add(Dense(self.hidden_layer_2_size, activation="relu"))
        model.add(Dense(self.action_size))
        model.compile(loss="mean_squared_error", optimizer=Adam(lr=self.learning_rate))
        return model

    def act(self, state, eps=0.):
        """Returns actions for given state as per current policy.

        Params
            ======
                state (array_like): current state
                eps (float): epsilon, for epsilon-greedy action selection
        """
        # Epsilon-greedy action selection
        if random.random() > eps:
            return np.argmax(self.model.predict(np.array([state])))
        else:
            return random.choice(np.arange(self.action_size))

    def step(self, state, action, reward, next_state, done, train=True):
        self.memory.append([state, action, reward, next_state, done])

        # Learn every UPDATE_EVERY time steps.
        ++self.t_step
        if (self.t_step % UPDATE_EVERY) == 0:
            # If enough samples are available in memory, get random subset and learn
            if len(self.memory) > BATCH_SIZE:
                experiences = random.sample(self.memory, BATCH_SIZE)
                if train:
                    self.learn(experiences, GAMMA)

    def learn2(self, experiences, gamma):

        """Update value parameters using given batch of experience tuples.

        Params
        ======
            experiences tuple of (s, a, r, s', done) tuples
            gamma (float): discount factor
        """
        # states, actions, rewards, next_states, dones = experiences
        # TODO Recheck and adjust below to optimize loop
        i=0
        number_of_epochs_per_fit=1
        for e in experiences:
            state, action, reward, new_state, done = e
            state_2_dim=np.array([state])
            target = self.target_model.predict(state_2_dim)
            if done:
                target[0][action] = reward
            else:
                new_state_2_dim=np.array([new_state])
                Q_future = max(self.target_model.predict(new_state_2_dim)[0])
                target[0][action] = reward + Q_future * GAMMA
            print("model.fit called {}".format(i))
            tb_callback_list=[self.create_tensorboard_callback(i)]
            self.model.fit(state_2_dim, target, epochs=number_of_epochs_per_fit, initial_epoch=i, verbose=0, callbacks=self.callbacks)
            i=i + number_of_epochs_per_fit
        self.target_train()

    def target_train(self):
        weights = self.model.get_weights()
        target_weights = self.target_model.get_weights()
        for i in range(len(target_weights)):
            target_weights[i] = weights[i] * TAU + target_weights[i] * (1 - TAU)
        self.target_model.set_weights(target_weights)
        pass


    def learn(self, experiences, gamma):
        dones = [e[4] for e in experiences if e is not None]
        dones = np.array(dones)

        actions = [e[1] for e in experiences if e is not None]
        actions = np.array(actions)

        rewards = [e[2] for e in experiences if e is not None]
        rewards = np.array(rewards)

        states=[e[0] for e in experiences if e is not None]
        states=np.array(states)

        new_states = [e[3] for e in experiences if e is not None]
        new_states = np.array(new_states)
        target = self.target_model.predict(states)

        indices_of_dones_with_true = np.where(dones == True)[0]
        indices_of_dones_with_false = np.where(dones == False)[0]  #use argwhere ?

        if indices_of_dones_with_true.shape[0] != 0:
            target[indices_of_dones_with_true, actions[indices_of_dones_with_true]] = rewards[indices_of_dones_with_true]

        if indices_of_dones_with_false.shape[0] != 0:
            Q_future = np.max(self.target_model.predict(new_states), axis=1)
            target[indices_of_dones_with_false, actions[indices_of_dones_with_false]] = \
                                            rewards[indices_of_dones_with_false] + Q_future[indices_of_dones_with_false] * GAMMA

        number_of_epochs_per_fit=2
        tb_callback_list = [self.create_tensorboard_callback(self.t_step)]
        self.model.fit(states, target, epochs=number_of_epochs_per_fit, verbose=0,
                       callbacks=[])
        self.target_train()
        pass

    def create_tensorboard_callback(self, run_id):
        log_sub_dir='C:\\devsbb\\git\\aicrowd\\baselines\\checkpoints\\run'+str(run_id)
        tensorboard_cb=TensorBoard(log_dir=log_sub_dir,
                                #histogram_freq=0,  # How often to log histogram visualizations
                                #embeddings_freq=0,  # How often to log embedding visualizations
                                #update_freq='epoch'
                                )
        return tensorboard_cb
