import matplotlib.pyplot as plt
from flatland.envs.malfunction_generators import malfunction_from_params
from flatland.envs.observations import TreeObsForRailEnv
from flatland.envs.rail_env import RailEnv
from flatland.envs.rail_generators import sparse_rail_generator
from flatland.envs.schedule_generators import sparse_schedule_generator
from flatland.utils.rendertools import RenderTool

from lupmodel.keras_model import KerasModel
from lupmodel.model_persister import ModelPersister
from torch_training.dueling_double_dqn import Agent
from utils.learning_utils import create_env_malfunction_parameters
from utils.learning_utils import get_state_size
from utils.learning_utils import run_trials

treeObservation = TreeObsForRailEnv(max_depth=2)
# Different agent types (trains) with different speeds.
speed_ration_map = {1.: 0.,  # Fast passenger train
                    1. / 2.: 1.0,  # Fast freight train
                    1. / 3.: 0.0,  # Slow commuter train
                    1. / 4.: 0.0}  # Slow freight train

# Parameters for the Environment
x_dim = 35
y_dim = 35
n_agents = 1

env = RailEnv(width=x_dim,
              height=y_dim,
              rail_generator=sparse_rail_generator(max_num_cities=3,
                                                   # Number of cities in map (where train stations are)
                                                   seed=1,  # Random seed
                                                   grid_mode=False,
                                                   max_rails_between_cities=2,
                                                   max_rails_in_city=3),
              schedule_generator=sparse_schedule_generator(speed_ration_map),
              number_of_agents=n_agents,
              malfunction_generator_and_process_data=malfunction_from_params(create_env_malfunction_parameters()),
              # Malfunction data generator
              obs_builder_object=treeObservation)
env.reset(True, True)
nb_actions= env.action_space[0]

# After training we want to render the results so we also load a renderer
env_renderer = RenderTool(env, gl="PILSVG", )
agent_obs = [None] * env.get_num_agents()
nb_trials=100
state_size=get_state_size(env, treeObservation)
agent = KerasModel(state_size, nb_actions)
print(state_size, nb_actions)
model_persister=ModelPersister()

scores= run_trials(env,env_renderer, agent_obs, treeObservation.max_depth, nb_trials, agent, model_persister)

# Plot overall training progress at the end
plt.plot(scores)
plt.show()

from tensorboard import program
tb = program.TensorBoard()
tb.configure(argv=[None, '--logdir', 'C:\\devsbb\\git\\aicrowd\\baselines\\checkpoints\\run1'])
url = tb.launch()
